require essioc
iocshLoad "${essioc_DIR}/common_config.iocsh"

epicsEnvSet IOC PBI-WS01:Ctrl-ECAT-100

require ecmccfg 8.0.0
iocshLoad "${ecmccfg_DIR}/startup.cmd" "ECMC_VER=8.0.2, NAMING=ESSnaming"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EK1100
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL1808
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL2819

# Axis 1 inserted switch
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput01,1)"
# Axis 1 extracted switch
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput02,1)"
# Axis 2 inserted switch
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput04,1)"
# Axis 2 extracted switch
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput05,1)"
# Axis 3 interlock
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput06,1)"
# Axis 3 inserted switch
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput07,1)"
# Axis 3 extracted switch
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput08,1)"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101

iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7047, CONFIG=-Motor-Nanotec-ST4118M1804-B"

ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,1200,2)"
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x3A,1,1)"

iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7047, CONFIG=-Motor-Nanotec-ST4118M1804-B"

ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,1200,2)"
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x3A,1,1)"

iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7047, CONFIG=-Motor-Nanotec-ST4118M1804-B"

# Reduced current: 1.2 A
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,1200,2)"
# Enable hardware interlock
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x32,1,1)"
# Use EL7047 brake control instead of ecmc's
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x3A,1,1)"

iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis1.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis2.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis3.ax"

iocshLoad "${ecmccfg_DIR}/applyConfig.cmd"
iocshLoad "${ecmccfg_DIR}/setAppMode.cmd"

dbLoadRecords "${E3_CMD_TOP}/db/alarms.db" "P=${SM_PREFIX}, MASTER_ID=${ECMC_EC_MASTER_ID}"
dbLoadRecords "${E3_CMD_TOP}/db/archiver.db" "P=${SM_PREFIX}"
dbLoadRecords "${E3_CMD_TOP}/db/brakes.db" "P=${SM_PREFIX}, MASTER_ID=${ECMC_EC_MASTER_ID}"
dbLoadRecords "${E3_CMD_TOP}/db/interlocks.db" "P=${SM_PREFIX}, MASTER_ID=${ECMC_EC_MASTER_ID}"
