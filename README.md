# MEBT WS (Medium Energy Beam Transfer Wire Scanner) EtherCAT IOC
- [Confluence page](https://confluence.esss.lu.se/x/1LW8G)
- [Interlock strategies](https://confluence.ess.eu/x/8DYAFQ)
- [EPLAN drawing](https://chess.esss.lu.se/enovia/link/ESS-5289042/21308.51166.29545.4610/valid)
```
Encoder: ???
Brake: ???
Motor: Nanotec ST4118M1804-B
Gearbox: ???
Limit switches: ???
Home switch: ???
```